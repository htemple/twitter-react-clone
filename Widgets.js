import React from 'react';
import "./Widgets.css";
import SearchIcon from "@material-ui/icons/Search";
import {TwitterTweetEmbed,
        TwitterTimelineEmbed,
        TwitterShareButton,} from "react-twitter-embed";

function Widgets() {
    return (
        <div className="widgets">

            <div className="widgets__input">

                <SearchIcon className="widgets__searchIcon" />
                <input placeholder="Keyword Search" type="text" />
               
            </div>

            <div className="widgets__widgetContainer">
                <div className="header__container">
                    <h2>Whats Happening?</h2>
                </div>
                <TwitterTweetEmbed tweetId={"1429111184813268996"} />

                 

                <TwitterShareButton
                url={"https://www.facebook.com/aaron.trepte"}
                    options={{ text: "#Buh-ROOOOOOOH, mayne", via: "Guyguy"}}
                />

                <TwitterTimelineEmbed 
                    sourceType="profile"
                    screenName="GameGrumps"
                    options={{height:400}}
                />
                <TwitterTimelineEmbed 
                    sourceType="profile"
                    screenName="OneyPlays"
                    options={{height:400}}
                />
            </div>

        </div>
    )

}

export default Widgets;