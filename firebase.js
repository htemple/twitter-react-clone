import { initializeApp } from 'firebase/app';
import * as firebase from 'firebase';
import { getFirestore, collection, getDocs } from 'firebase/firestore';


const firebaseConfig = {
    apiKey: "AIzaSyDYWL153XyRanvSrfcdMFjVqZAXlmQy8Fk",
    authDomain: "twitter-clone-cb7eb.firebaseapp.com",
    projectId: "twitter-clone-cb7eb",
    storageBucket: "twitter-clone-cb7eb.appspot.com",
    messagingSenderId: "660488932919",
    appId: "1:660488932919:web:0404c70dce1cb3a50c79b2",
    measurementId: "G-DF88W8WE3E"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

const db = firebaseApp.firestore();


export default db;