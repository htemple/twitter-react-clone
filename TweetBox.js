import React, {useState} from 'react';
import "./TweetBox.css";
import { Avatar, Button } from "@material-ui/core";
import db from './firebase';

function TweetBox() {
    const [tweetMessage,setTweetMessage] = useState("");
    const [tweetImage, setTweetImage] = useState("");

    const sendTweet = e => {
        e.preventDefault(); //prevents refresh

        db.collection('posts').add({
            displayName: 'BruhBroham',
            userName: 'Broham',
            verified: true,
            text: tweetMessage,
            avatar: "https://picon.ngfiles.com/158000/flash_158417_card.png?f1601495530",
            image: tweetImage

        });

        setTweetMessage("");
        setTweetImage("");
    };

    return (
        <div className="tweetBox">
            <form>
                <div className="tweetBox__input">
                    <Avatar src="https://picon.ngfiles.com/158000/flash_158417_card.png?f1601495530"/>
                    <input 
                        onChange={ (e) => setTweetMessage(e.target.value)}
                        value={tweetMessage} 
                        placeholder="What's up, bud?" 
                        type="text" 
                    />
                    
                </div>
                <input 
                    value={tweetImage}
                    onChange={(e) => setTweetImage(e.target.value)}
                    className="tweetBox__imageInput"
                    placeholder="Enter your image URL" 
                    type="text" 
                />

                <Button onClick={sendTweet} type="submit" className="tweetBox__tweetButton">Tweet</Button>
            </form>
        </div>
    )
}


export default TweetBox;